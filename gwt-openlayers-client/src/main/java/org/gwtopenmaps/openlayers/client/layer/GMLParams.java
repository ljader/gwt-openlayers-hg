/**
 * 
 */
package org.gwtopenmaps.openlayers.client.layer;

import org.gwtopenmaps.openlayers.client.util.JSObject;

/**
 * @author Francesco
 * 
 */
public class GMLParams extends Params {

	protected GMLParams(JSObject jsObject) {
		super(jsObject);
	}

}
