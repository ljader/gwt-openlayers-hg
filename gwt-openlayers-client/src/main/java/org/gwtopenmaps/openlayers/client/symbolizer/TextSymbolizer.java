/**
 * 
 */
package org.gwtopenmaps.openlayers.client.symbolizer;

import org.gwtopenmaps.openlayers.client.util.JSObject;

/**
 * @author lorenzo
 * 
 */
public class TextSymbolizer extends Symbolizer {

	protected TextSymbolizer(JSObject jsObject) {
		super(jsObject);
		// TODO Auto-generated constructor stub
	}

	public TextSymbolizer(TextSymbolizerOptions options) {
		this(TextSymbolizerImpl.create(options.getJSObject()));
	}
}
